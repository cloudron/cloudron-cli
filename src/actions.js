'use strict';

const assert = require('assert'),
    config = require('./config.js'),
    ejs = require('ejs'),
    { exit, locateManifest } = require('./helper.js'),
    { EventSource } = require('eventsource'),
    fs = require('fs'),
    https = require('https'),
    LineStream = require('./line-stream.js'),
    manifestFormat = require('cloudron-manifestformat'),
    os = require('os'),
    path = require('path'),
    readline = require('./readline.js'),
    safe = require('safetydance'),
    spawn = require('child_process').spawn,
    semver = require('semver'),
    superagent = require('./superagent.js'),
    Table = require('easy-table'),
    tar = require('tar-fs'),
    timers = require('timers/promises'),
    zlib = require('zlib');

exports = module.exports = {
    list,
    login,
    logout,
    open,
    install,
    setLocation,
    debug,
    update,
    uninstall,
    logs,
    exec,
    status,
    inspect,
    pull,
    push,
    restart,
    start,
    stop,
    repair,
    init,
    restore,
    importApp,
    exportApp,
    clone,
    backupCreate,
    backupList,
    cancel,

    envGet,
    envSet,
    envList,
    envUnset
};

const NO_APP_FOUND_ERROR_STRING = 'Could not determine app. Use --app to specify the app\n';

// options for the request module
function requestOptions(options) {
    const adminFqdn = options.server || config.apiEndpoint();

    // ensure config can return the correct section
    config.setActive(adminFqdn);

    const token = options.token || config.token();
    const rejectUnauthorized = !(options.allowSelfsigned || options.acceptSelfsigned || config.allowSelfsigned());

    if (!adminFqdn && !token) return exit('Login with "cloudron login" first'); // a bit rough to put this here!

    return { adminFqdn, token, rejectUnauthorized };
}

function createRequest(method, apiPath, options) {
    const { adminFqdn, token, rejectUnauthorized } = requestOptions(options);

    let url = `https://${adminFqdn}${apiPath}`;
    if (url.includes('?')) url += '&'; else url += '?';
    url += `access_token=${token}`;
    const request = superagent.request(method, url);
    if (!rejectUnauthorized) request.disableTLSCerts();
    request.retry(3);
    request.ok(() => true);
    return request;
}

// error for the request module
function requestError(response) {
    if (response.status === 401) return 'Invalid token. Use cloudron login again.';

    return `${response.status} message: ${response.body.message || JSON.stringify(response.body)}`; // body is sometimes just a string like in 401
}

async function selectDomain(location, options) {
    assert.strictEqual(typeof location, 'string');
    assert.strictEqual(typeof options, 'object');

    const { adminFqdn } = requestOptions(options);

    const response = await createRequest('GET', '/api/v1/domains', options);
    if (response.status !== 200) throw new Error(`Failed to list domains: ${requestError(response)}`);

    const domains = response.body.domains;

    let domain;
    let subdomain = location;
    // find longest matching domain name. this maps to DNS where subdomain is a zone of it's own
    const matchingDomain = domains
        .map(function (d) { return d.domain; } )
        .sort(function(a, b) { return b.length - a.length; })
        .find(function (d) { return location.endsWith(d); });

    if (matchingDomain) {
        domain = matchingDomain;
        subdomain = location.slice(0, -matchingDomain.length-1);
    } else { // use the admin domain
        domain = domains
            .map(function (d) { return d.domain; } )
            .sort(function(a, b) { return b.length - a.length; })
            .find(function (d) { return adminFqdn.endsWith(d); });
    }

    return { subdomain, domain };
}

async function stopActiveTask(app, options) {
    assert.strictEqual(typeof app, 'object');
    assert.strictEqual(typeof options, 'object');

    if (!app.taskId) return;

    console.log(`Stopping app's current active task ${app.taskId}`);

    const response = await createRequest('POST', `/api/v1/tasks/${app.taskId}/stop`, options);
    if (response.status !== 204) throw `Failed to stop active task: ${requestError(response)}`;
}

async function selectAppWithRepository(repository, options) {
    assert.strictEqual(typeof repository, 'string');
    assert.strictEqual(typeof options, 'object');

    const response = await createRequest('GET', '/api/v1/apps', options);
    if (response.status !== 200) throw new Error(`Failed to install app: ${requestError(response)}`);

    const matchingApps = response.body.apps.filter(function (app) {
        return !app.appStoreId && app.manifest.dockerImage.startsWith(repository); // never select apps from the store
    });

    if (matchingApps.length === 0) return [ ];
    if (matchingApps.length === 1) return matchingApps[0];

    console.log();
    console.log('Available apps using same repository %s:', repository);
    matchingApps.sort(function (a, b) { return a.fqdn < b.fqdn ? -1 : 1; });
    matchingApps.forEach(function (app, index) {
        console.log('[%s]\t%s', index, app.fqdn);
    });

    let index = -1;
    while (true) {
        index = parseInt(await readline.question('Choose app [0-' + (matchingApps.length-1) + ']: ', {}), 10);
        if (isNaN(index) || index < 0 || index > matchingApps.length-1) console.log('Invalid selection');
        else break;
    }

    return matchingApps[index];
}

// appId may be the appId or the location
async function getApp(options) {
    assert.strictEqual(typeof options, 'object');

    const app = options.app || null;

    if (!app) { // determine based on repository name given during 'build'
        const manifestFilePath = locateManifest();

        if (!manifestFilePath) throw new Error(NO_APP_FOUND_ERROR_STRING);

        const sourceDir = path.dirname(manifestFilePath);
        const appConfig = config.getAppConfig(sourceDir);

        if (!appConfig.repository) throw new Error(NO_APP_FOUND_ERROR_STRING);

        const [error, result] = await safe(selectAppWithRepository(appConfig.repository, options));
        if (error || result.length === 0) return null;

        return result;
    } else if (app.match(/.{8}-.{4}-.{4}-.{4}-.{8}/)) {  // it is an id
        const response = await createRequest('GET', `/api/v1/apps/${app}`, options);
        if (response.status !== 200) throw new Error(`Failed to get app: ${requestError(response)}`);

        return response.body;
    } else { // it is a location
        const response = await createRequest('GET', '/api/v1/apps', options);
        if (response.status !== 200) throw new Error(`Failed to get apps: ${requestError(response)}`);

        const match = response.body.apps.filter(function (m) { return m.subdomain === app || m.location === app || m.fqdn === app; });
        if (match.length == 0) throw new Error(`App at location ${app} not found`);

        return match[0];
    }
}

async function waitForHealthy(appId, options) {
    assert.strictEqual(typeof appId, 'string');
    assert.strictEqual(typeof options, 'object');

    if (!options.wait) return;

    process.stdout.write('\n => ' + 'Wait for health check ');

    while (true) {
        await timers.setTimeout(1000);
        const response = await createRequest('GET', `/api/v1/apps/${appId}`, options);
        if (response.status !== 200) throw new Error(`Failed to get app: ${requestError(response)}`);

        // do not check installation state here. it can be pending_backup etc (this is a bug in box code)
        if (response.body.health === 'healthy') return;

        // skip health check if app is in debug mode
        if (response.body.debugMode !== null) {
            process.stdout.write(' (skipped in debug mode)');
            return;
        }

        process.stdout.write('.');
    }
}

async function waitForTask(taskId, options) {
    assert.strictEqual(typeof taskId, 'string');
    assert.strictEqual(typeof options, 'object');

    let currentMessage = '';

    while (true) {
        const response = await createRequest('GET', `/api/v1/tasks/${taskId}`, options);
        if (response.status !== 200) throw new Error(`Failed to get task: ${requestError(response)}`);

        if (!response.body.active) return response.body;

        let message = response.body.message || '';

        // Backup downloading includes the filename and clutters the cli output
        if (message.indexOf('Restore - Down') === 0) {
            message = 'Downloading backup';
        }

        // track current progress and show progress dots
        if (currentMessage && currentMessage === message) {
            if (currentMessage.indexOf('Creating image') === -1 && currentMessage.indexOf('Downloading backup') === -1) process.stdout.write('.');
        } else if (message) {
            process.stdout.write('\n => ' + message.trim() + ' ');
        } else {
            process.stdout.write('\n => ' + 'Waiting to start installation ');
        }

        currentMessage = message;

        await timers.setTimeout(1000);
    }
}

async function waitForFinishInstallation(appId, taskId, options) {
    assert.strictEqual(typeof appId, 'string');
    assert.strictEqual(typeof taskId, 'string');
    assert.strictEqual(typeof options, 'object');

    await waitForTask(taskId, options);

    const response = await createRequest('GET', `/api/v1/apps/${appId}`, options);
    if (response.status !== 200) throw new Error(`Failed to get app: ${requestError(response)}`);

    if (response.body.installationState !== 'installed') throw new Error(`Installation failed: ${response.body.error ? response.body.error.message : ''}`);

    await waitForHealthy(appId, options);
}

async function waitForFinishBackup(appId, taskId, options) {
    assert.strictEqual(typeof appId, 'string');
    assert.strictEqual(typeof taskId, 'string');
    assert.strictEqual(typeof options, 'object');

    const result  = await waitForTask(taskId, options);

    if (result.error) throw new Error(`Backup failed: ${result.error.message}`);

    const response = await createRequest('GET', `/api/v1/apps/${appId}`, options);
    if (response.status !== 200) throw new Error(`Failed to get app: ${requestError(response)}`);
}

async function stopApp(app, options) {
    assert.strictEqual(typeof app, 'object');
    assert.strictEqual(typeof options, 'object');

    const response = await createRequest('POST', `/api/v1/apps/${app.id}/stop`, options);
    if (response.status !== 202) throw `Failed to stop app: ${requestError(response)}`;

    await waitForTask(response.body.taskId, options);
}

async function startApp(app, options) {
    assert.strictEqual(typeof app, 'object');
    assert.strictEqual(typeof options, 'object');

    const response = await createRequest('POST', `/api/v1/apps/${app.id}/start`, options);
    if (response.status !== 202) throw `Failed to start app: ${requestError(response)}`;

    await waitForTask(response.body.taskId, options);
}

async function restartApp(app, options) {
    assert.strictEqual(typeof app, 'object');
    assert.strictEqual(typeof options, 'object');

    const response = await createRequest('POST', `/api/v1/apps/${app.id}/restart`, options);
    if (response.status !== 202) throw `Failed to restart app: ${requestError(response)}`;

    await waitForTask(response.body.taskId, options);
}

async function authenticate(adminFqdn, username, password, options) {
    assert.strictEqual(typeof adminFqdn, 'string');
    assert.strictEqual(typeof username, 'string');
    assert.strictEqual(typeof password, 'string');
    assert.strictEqual(typeof options, 'object');

    let totpToken;

    const { rejectUnauthorized, askForTotpToken } = options;
    if (askForTotpToken) totpToken = await readline.question('2FA Token: ', {});

    const request = superagent.post(`https://${adminFqdn}/api/v1/auth/login`)
        .timeout(60000)
        .send({ username, password, totpToken, type: 'cid-cli' })
        .ok(() => true)
        .set('User-Agent', 'cloudron-cli');
    if (!rejectUnauthorized) request.disableTLSCerts();
    const response = await request; // this triggers the request
    if (response.status === 401) {
        if (response.body.message === 'A totpToken must be provided') {
            return await authenticate(adminFqdn, username, password, { rejectUnauthorized, askForTotpToken: true });
        } else if (response.body.message === 'Invalid totpToken') {
            console.log('Invalid 2FA Token');
            return await authenticate(adminFqdn, username, password, { rejectUnauthorized, askForTotpToken: true });
        } else {
            throw new Error('Invalid credentials');
        }
    }

    if (response.status !== 200) throw new Error(`Login failed: Status code: ${requestError(response)}`);

    return response.body.accessToken;
}

async function login(adminFqdn, localOptions, cmd) {
    if (!adminFqdn) adminFqdn = await readline.question('Cloudron Domain (e.g. my.example.com): ', {});
    if (!adminFqdn) return exit('');

    if (adminFqdn.indexOf('https://') === 0) adminFqdn = adminFqdn.slice('https://'.length);
    if (adminFqdn.indexOf('/') !== -1) adminFqdn = adminFqdn.slice(0, adminFqdn.indexOf('/'));

    config.setActive(adminFqdn);

    const options = cmd.optsWithGlobals();

    const rejectUnauthorized = !(options.allowSelfsigned || options.acceptSelfsigned);
    let token = config.token();
    if (token) { // check if the token is not expired
        const request = superagent.get(`https://${adminFqdn}/api/v1/profile?access_token=${token}`)
            .timeout(60000)
            .ok(() => true);
        if (!rejectUnauthorized) request.disableTLSCerts();

        const [error, response] = await safe(request);
        if (error) return exit(error);
        if (response.status === 200) {
            console.log('Existing token still valid.');
        } else {
            token = null;
            console.log(`Existing token possibly expired: ${requestError(response)}`);
        }
    }

    if (!token) {
        const username = options.username || await readline.question('Username: ', {});
        const password = options.password || await readline.question('Password: ', { noEchoBack: true });

        const [error, result] = await safe(authenticate(adminFqdn, username, password, { rejectUnauthorized, askForTotpToken: false }));
        if (error) return exit(`Failed to login: ${error.message}`);
        token = result;
    }

    config.setActive(adminFqdn);
    config.setApiEndpoint(adminFqdn);
    config.setToken(token);
    config.setAllowSelfsigned(cmd.optsWithGlobals().allowSelfsigned || cmd.optsWithGlobals().acceptSelfsigned);
    config.set('cloudrons.default', adminFqdn);

    console.log('Login successful.');
}

function logout() {
    config.setToken(null);
    config.set('cloudrons.default', null);

    console.log('Logged out.');
}

async function open(localOptions, cmd) {
    const options = cmd.optsWithGlobals();
    const [error, app] = await safe(getApp(options));
    if (error) return exit(error);

    if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

    // pure esm module
    (await import('open')).default(`https://${app.fqdn}`);
}

async function list(localOptions, cmd) {
    const options = cmd.optsWithGlobals();
    const [error, response] = await safe(createRequest('GET', '/api/v1/apps', options));
    if (error) return exit(error);
    if (response.status !== 200) return exit(`Failed to list apps: ${requestError(response)}`);

    let apps = response.body.apps;

    if (options.tag) apps = apps.filter(function (a) { return a.tags.indexOf(options.tag) !== -1; });

    if (options.quiet) {
        console.log(apps.map(a => a.id).join('\n'));
        return;
    }

    // after quiet
    if (apps.length === 0) return console.log('No apps installed.');

    const t = new Table();

    const responses = await Promise.allSettled(apps.map(function (app) { return createRequest('GET', `/api/v1/apps/${app.id}`, options); }));
    responses.forEach(function (result) {
        if (result.status !== 'fulfilled') return exit(`Failed to list app: ${requestError(result.value)}`);

        const response = result.value;

        if (response.status !== 200) return exit(`Failed to list app: ${requestError(response)}`);
        response.body.location = response.body.location || response.body.subdomain; // LEGACY support

        const detailedApp = response.body;

        t.cell('Id', detailedApp.id);
        t.cell('Location', detailedApp.fqdn);
        t.cell('Manifest Id', (detailedApp.manifest.id || 'customapp') + '@' + detailedApp.manifest.version);
        let prettyState;
        if (detailedApp.installationState === 'installed') {
            prettyState = (detailedApp.debugMode ? 'debug' : detailedApp.runState);
        } else if (detailedApp.installationState === 'error') {
            prettyState = `error (${detailedApp.error.installationState})`;
        } else {
            prettyState = detailedApp.installationState;
        }
        t.cell('State', prettyState);
        t.newRow();
    });

    console.log();
    console.log(t.toString());
}

async function querySecondaryDomains(app, manifest, options) {
    const secondaryDomains = {};

    if(!manifest.httpPorts) return secondaryDomains;

    for (const env in manifest.httpPorts) {
        const defaultDomain = (app && app.secondaryDomains && app.secondaryDomains[env]) ? app.secondaryDomains[env] : (manifest.httpPorts[env].defaultValue || '');
        const input = await readline.question(`${manifest.httpPorts[env].description} (default: "${defaultDomain}"): `, {});
        secondaryDomains[env] = await selectDomain(input, options);
    }
    return secondaryDomains;
}

async function queryPortBindings(app, manifest) {
    const portBindings = {};
    const allPorts = Object.assign({}, manifest.tcpPorts, manifest.udpPorts);

    for (const env in allPorts) {
        const defaultPort = (app && app.portBindings && app.portBindings[env]) ? app.portBindings[env] : (allPorts[env].defaultValue || '');
        const port = await readline.question(allPorts[env].description + ' (default ' + env + '=' + defaultPort + '. "x" to disable): ', {});
        if (port === '') {
            portBindings[env] = defaultPort;
        } else if (isNaN(parseInt(port, 10))) {
            console.log(('Cleared ' + env).gray);
        } else {
            portBindings[env] = parseInt(port, 10);
        }
    }
    return portBindings;
}

async function downloadManifest(appstoreId) {
    const [ id, version ] = appstoreId.split('@');

    if (!manifestFormat.isId(id)) throw new Error('Invalid appstore ID');
    if (version && !semver.valid(version)) throw new Error('Invalid appstore version, not a semver');

    // encodeURIComponent required to protect against passing junk in the URL (e.g appstoreId=org.wordpress.cloudronapp?foo=bar)
    const url = config.appStoreOrigin() + '/api/v1/apps/' + id + (version ? '/versions/' + version : '');

    const [error, response] = await safe(superagent.get(url).ok(() => true));
    if (error) throw new Error(`Failed to list apps from appstore: ${error.message}`);
    if (response.status !== 200) throw new Error(`Failed to get app info from store: ${requestError(response)}`);

    return { manifest: response.body.manifest, manifestFilePath: null /* manifest file path */ };
}

async function getManifest(appstoreId) {
    assert.strictEqual(typeof appstoreId, 'string');

    if (appstoreId) return await downloadManifest(appstoreId);

    const manifestFilePath = locateManifest();
    if (!manifestFilePath) throw new Error('No CloudronManifest.json found');

    const result = manifestFormat.parseFile(manifestFilePath);
    if (result.error) throw new Error(`Invalid CloudronManifest.json: ${result.error.message}`);

    // resolve post install message
    if (result.manifest.postInstallMessage && result.manifest.postInstallMessage.slice(0, 7) === 'file://') {
        let postInstallFilename = result.manifest.postInstallMessage.slice(7);
        // resolve filename wrt manifest
        if (manifestFilePath) postInstallFilename = path.resolve(path.dirname(manifestFilePath), postInstallFilename);
        result.manifest.postInstallMessage = safe.fs.readFileSync(postInstallFilename, 'utf8');
        if (!result.manifest.postInstallMessage) return exit('Could not read postInstallMessage file ' + postInstallFilename + ':' + safe.error.message);
    }

    return { manifest: result.manifest, manifestFilePath };
}

async function install(localOptions, cmd) {
    const options = cmd.optsWithGlobals();

    try {
        const result = await getManifest(options.appstoreId || '');
        const { manifest, manifestFilePath } = result;

        if (!manifest.dockerImage) { // not a manifest from appstore
            const sourceDir = path.dirname(manifestFilePath);
            const image = options.image || config.getAppConfig(sourceDir).dockerImage;

            if (!image) return exit('No image found, please run `cloudron build` first or specify using --image');

            manifest.dockerImage = image;
        }

        const location = options.location || await readline.question('Location: ', {});
        if (!location) return exit('');

        const domainObject = await selectDomain(location, options);

        // secondary domains
        let secondaryDomains = {};
        if (options.secondaryDomains) {
            // ask the user for port values if the ports are different in the app and the manifest
            if (typeof options.secondaryDomains === 'string') {
                secondaryDomains = {};
                for (const kv of options.secondaryDomains.split(',')) {
                    const tmp = kv.split('=');
                    secondaryDomains[tmp[0]] = await selectDomain(tmp[1], options);
                }
            } else {
                secondaryDomains = await querySecondaryDomains(null /* existing app */, manifest, options);
            }
        } else if (manifest.httpPorts) { // just put in defaults
            for (const env in manifest.httpPorts) {
                secondaryDomains[env] = await selectDomain(manifest.httpPorts[env].defaultValue, options);
            }
        }

        for (const binding in secondaryDomains) console.log(`Secondary domain ${binding}: ${secondaryDomains[binding].subdomain}.${secondaryDomains[binding].domain}`);

        const aliasDomains = [];
        if (options.aliasDomains) {
            for (const aliasDomain of options.aliasDomains.split(',')) {
                aliasDomains.push(await selectDomain(aliasDomain, options));
            }
        }

        // port bindings
        let ports = {};
        if (options.portBindings) {
            // ask the user for port values if the ports are different in the app and the manifest
            if (typeof options.portBindings === 'string') {
                ports = { };
                options.portBindings.split(',').forEach(function (kv) {
                    const tmp = kv.split('=');
                    if (isNaN(parseInt(tmp[1], 10))) return; // disable the port
                    ports[tmp[0]] = parseInt(tmp[1], 10);
                });
            } else {
                ports = await queryPortBindings(null /* existing app */, manifest);
            }
        } else { // just put in defaults
            const allPorts = Object.assign({}, manifest.tcpPorts, manifest.udpPorts);
            for (const portName in allPorts) {
                ports[portName] = allPorts[portName].defaultValue;
            }
        }

        for (const port in ports) console.log(`Port ${port}: ${ports[port]}`);

        const data = {
            appStoreId: options.appstoreId || '', // note case change
            manifest: options.appstoreId ? null : manifest, // cloudron ignores manifest anyway if appStoreId is set
            location: domainObject.subdomain, // LEGACY
            subdomain: domainObject.subdomain,
            domain: domainObject.domain,
            secondaryDomains,
            aliasDomains,
            ports,
            accessRestriction: null
        };

        // the sso only applies for apps which allow optional sso
        if (manifest.optionalSso) data.sso = options.sso;

        if (options.debug) { // 'true' when no args.  otherwise, array
            const debugCmd = options.debug === true ? [ '/bin/bash', '-c', 'echo "Repair mode. Use the webterminal or cloudron exec to repair. Sleeping" && sleep infinity' ] : options.debug;
            data.debugMode = {
                readonlyRootfs: options.readonly ? true : false,
                cmd: debugCmd
            };
            data.memoryLimit = -1;
            options.wait = false; // in debug mode, health check never succeeds
        }

        if (!options.appstoreId && manifest.icon) {
            let iconFilename = manifest.icon.slice(0, 7) === 'file://' ? manifest.icon.slice(7) : manifest.icon;
            iconFilename = path.resolve(path.dirname(manifestFilePath), iconFilename); // resolve filename wrt manifest
            data.icon = safe.fs.readFileSync(iconFilename, { encoding: 'base64' });
            if (!data.icon) return exit(`Could not read icon: ${iconFilename} message: ${safe.error.message}`);
        }

        const request = createRequest('POST', '/api/v1/apps/install', options);
        const response = await request.send(data);
        if (response.status !== 202) return exit(`Failed to install app: ${requestError(response)}`);

        const appId = response.body.id;

        console.log('App is being installed.');

        await waitForFinishInstallation(appId, response.body.taskId, options);
        console.log('\n\nApp is installed.');
    } catch (error) {
        exit('\n\nApp installation error: %s', error.message);
    }
}

async function setLocation(localOptions, cmd) {
    const options = cmd.optsWithGlobals();

    try {
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

        if (!options.location) options.location = await readline.question(`Enter new location (default: ${app.fqdn}): `, { });
        const location = options.location || app.subdomain;

        const domainObject = await selectDomain(location, options);

        const secondaryDomains = {};
        if (app.secondaryDomains) { // only valid post 7.1
            app.secondaryDomains.forEach(sd => {
                secondaryDomains[sd.environmentVariable] = {
                    subdomain: sd.subdomain,
                    domain: sd.domain
                };
            });
        }

        let aliasDomains = app.aliasDomains;
        if (options.aliasDomains) {
            aliasDomains = [];
            for (const aliasDomain of options.aliasDomains.split(',')) {
                aliasDomains.push(await selectDomain(aliasDomain, options));
            }
        }

        const ports = {};
        for (const portName in app.portBindings) {
            ports[portName] = app.portBindings[portName].hostPort;
        }

        const data = {
            location: domainObject.subdomain, // LEGACY
            subdomain: domainObject.subdomain,
            domain: domainObject.domain,
            ports,
            secondaryDomains,
            aliasDomains
        };

        // secondary domains
        if (options.secondaryDomains) {
            // ask the user for port values if the ports are different in the app and the manifest
            if (typeof options.secondaryDomains === 'string') {
                data.secondaryDomains = {};
                for (const kv of options.secondaryDomains.split(',')) {
                    const tmp = kv.split('=');
                    data.secondaryDomains[tmp[0]] = await selectDomain(tmp[1], options);
                }
            } else {
                data.secondaryDomains = await querySecondaryDomains(app, app.manifest, options);
            }

            for (const binding in data.secondaryDomains) console.log(`Secondary domain ${binding}: ${data.secondaryDomains[binding].subdomain}.${data.secondaryDomains[binding].domain}`);
        }

        // port bindings
        if (options.portBindings) {
            let ports;
            // ask the user for port values if the ports are different in the app and the manifest
            if (typeof options.portBindings === 'string') {
                ports = {};
                options.portBindings.split(',').forEach(function (kv) {
                    const tmp = kv.split('=');
                    if (isNaN(parseInt(tmp[1], 10))) return; // disable the port
                    ports[tmp[0]] = parseInt(tmp[1], 10);
                });
            } else {
                ports = await queryPortBindings(app, app.manifest);
            }

            for (const port in ports) {
                console.log('%s: %s', port, ports[port]);
            }

            data.ports = ports;
        }

        const request = createRequest('POST', `/api/v1/apps/${app.id}/configure/location`, options);
        const response = await request.send(data);
        if (response.status !== 202) return exit(`Failed to configure app: ${requestError(response)}`);

        await waitForTask(response.body.taskId, options);
        await waitForHealthy(app.id, options);
        console.log('\n\nApp configured');
    } catch (error) {
        exit(error);
    }
}

async function update(localOptions, cmd) {
    const options = cmd.optsWithGlobals();

    try {
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

        const result = await getManifest(options.appstoreId || '');
        const { manifest, manifestFilePath } = result;

        let data = {};

        if (!manifest.dockerImage) { // not a manifest from appstore
            const sourceDir = path.dirname(manifestFilePath);
            const image = options.image || config.getAppConfig(sourceDir).dockerImage;

            if (!image) return exit('No image found, please run `cloudron build` first or use --image');

            manifest.dockerImage = image;

            if (manifest.icon) {
                let iconFilename = manifest.icon.slice(0, 7) === 'file://' ? manifest.icon.slice(7) : manifest.icon;
                iconFilename = path.resolve(path.dirname(manifestFilePath), iconFilename); // resolve filename wrt manifest
                data.icon = safe.fs.readFileSync(iconFilename, { encoding: 'base64' });
                if (!data.icon) return exit(`Could not read icon: ${iconFilename} message: ${safe.error.message}`);
            }
        }

        let apiPath;

        if (app.error && (app.error.installationState === 'pending_install')) { // install had failed. call repair to re-install
            apiPath = `/api/v1/apps/${app.id}/repair`;
            data = Object.assign(data, { manifest });
        } else {
            apiPath = `/api/v1/apps/${app.id}/update`;
            data = Object.assign(data, {
                appStoreId: options.appstoreId || '', // note case change
                manifest: manifest,
                skipBackup: !options.backup,
                skipNotification: true,
                force: true // permit downgrades
            });
        }

        const request = createRequest('POST', apiPath, options);
        const response = await request.send(data);
        if (response.status !== 202) return exit(`Failed to update app: ${requestError(response)}`);

        process.stdout.write('\n => ' + 'Waiting for app to be updated ');

        await waitForFinishInstallation(app.id, response.body.taskId, options);
        console.log('\n\nApp is updated.');
    } catch (error) {
        console.log(error);
        exit(`\n\nApp update error: ${error.message}`);
    }
}

async function debug(args, localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

        const debugCmd = args.length === 0 ? [ '/bin/bash', '-c', 'echo "Repair mode. Use the webterminal or cloudron exec to repair. Sleeping" && sleep infinity' ] : args;

        const data = {
            debugMode: options.disable ? null : {
                readonlyRootfs: options.readonly ? true : false,
                cmd: debugCmd
            }
        };

        const request = createRequest('POST', `/api/v1/apps/${app.id}/configure/debug_mode`, options);
        const response = await request.send(data);
        if (response.status !== 202) return exit(`Failed to set debug mode: ${requestError(response)}`);

        await waitForTask(response.body.taskId, options);

        const memoryLimit = options.limitMemory ? 0 : -1;

        // skip setting memory limit if unchanged
        if (app.memoryLimit === memoryLimit) return console.log('\n\nDone');

        console.log('\n');
        console.log(options.limitMemory || options.disable ? 'Limiting memory' : 'Setting unlimited memory');

        const request2 = createRequest('POST', `/api/v1/apps/${app.id}/configure/memory_limit`, options);
        const response2 = await request2.send({ memoryLimit });
        if (response2.status !== 202) return exit(`Failed to set memory limit: ${requestError(response2)}`);

        await waitForTask(response2.body.taskId, options);
        console.log('\n\nDone');
    } catch (error) {
        exit(error);
    }
}

async function repair(localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

        const data = {};
        if (options.image) data.dockerImage = options.image;

        const request = createRequest('POST', `/api/v1/apps/${app.id}/repair`, options);
        const response = await request.send(data);
        if (response.status !== 202) return exit(`Failed to set repair mode: ${requestError(response)}`);

        process.stdout.write('\n => ' + 'Waiting for app to be repaired ');

        await waitForFinishInstallation(app.id, response.body.taskId, options);
        console.log('\n\nApp is repaired.');
    } catch (error) {
        exit('App repair error: %s', error.message);
    }
}

async function cancel(localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);
        if (!app.taskId) return exit('No active task.');
        await stopActiveTask(app, options);
        console.log('\nTask stopped.');
    } catch (error) {
        exit(error);
    }
}

async function uninstall(localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

        await stopActiveTask(app, options);

        const response = await createRequest('POST', `/api/v1/apps/${app.id}/uninstall`, options);
        if (response.status !== 202) return exit(`Failed to uninstall app: ${requestError(response)}`);

        process.stdout.write('\n => ' + 'Waiting for app to be uninstalled ');

        await waitForTask(response.body.taskId, options);
        const response2 = await createRequest('GET', `/api/v1/apps/${app.id}`, options);
        if (response2.status === 404) {
            console.log('\n\nApp %s successfully uninstalled.', app.fqdn);
        } else if (response2.body.installationState === 'error') {
            console.log('\n\nApp uninstallation failed.\n');
            exit(response2.body.errorMessage);
        }
    } catch (error) {
        exit(error);
    }
}

function logPrinter(obj) {
    let message;

    if (obj.message === null) {
        message = '[large binary blob skipped]';
    } else if (typeof obj.message === 'string') {
        message = obj.message;
    } else if (Array.isArray(obj.message)) {
        message = Buffer.from(obj.message).toString('utf8');
    }

    const ts = new Date(obj.realtimeTimestamp/1000).toTimeString().split(' ')[0];
    console.log('%s - %s', ts, message);
}

async function logs(localOptions, cmd) {
    const options = cmd.optsWithGlobals();
    const { adminFqdn, token, rejectUnauthorized } = requestOptions(options);
    const lines = options.lines || 500;
    const tail = !!options.tail;
    let apiPath;

    if (typeof options.system === 'boolean' && options.system) {
        // box
        apiPath = `https://${adminFqdn}/api/v1/system/${ tail ? 'logstream' : 'logs' }/box`;
    } else if (typeof options.system === 'string') {
        // services
        apiPath = `https://${adminFqdn}/api/v1/services/${options.system}/${ tail ? 'logstream' : 'logs' }`;
    } else {
        // apps
        const [error, app] = await safe(getApp(options));
        if (error) return exit(error);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

        apiPath = `https://${adminFqdn}/api/v1/apps/${app.id}/${ tail ? 'logstream' : 'logs' }`;
    }

    if (tail) {
        const url = `${apiPath}?access_token=${token}&lines=10&format=json`;
        const es = new EventSource(url, { rejectUnauthorized }); // not sure why this is needed

        es.addEventListener('message', function (e) { // e { type, data, lastEventId }. lastEventId is the timestamp
            logPrinter(JSON.parse(e.data));
        });

        es.addEventListener('error', function (error) {
            if (error.status === 401) return exit('Please login first');
            if (error.status === 412) exit('Logs currently not available.');
            exit(error);
        });
    } else {
        const url = `${apiPath}?access_token=${token}&lines=${lines}&format=json`;

        const req = superagent.get(url, { rejectUnauthorized });
        req.on('response', function (response) {
            if (response.status !== 200) return exit(`Failed to get logs: ${requestError(response)}`);
        });
        req.on('error', (error) => exit(`Pipe error: ${error.message}`));

        const lineStream = new LineStream();
        lineStream
            .on('line', (line) => { logPrinter(JSON.parse(line)); } )
            .on('error', (error) => exit(`JSON parse error: ${error.message}`))
            .on('end', process.exit);

        req.pipe(lineStream);
    }
}

async function status(localOptions, cmd) {
    const options = cmd.optsWithGlobals();
    const [error, app] = await safe(getApp(options));
    if (error) return exit(error);
    if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

    console.log();
    console.log('Id: \t\t', app.id);
    console.log('Location: \t', app.fqdn);
    console.log('Version: \t', app.manifest.version);
    console.log('Manifest Id: \t', (app.manifest.id || 'customapp'));
    console.log('Docker image: \t', (app.manifest.dockerImage));
    console.log('Install state: \t', (app.installationState + (app.debugMode ? ' (debug)' : '')));
    console.log('Run state: \t', app.runState);
    console.log();
}

async function inspect(localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const response = await createRequest('GET', '/api/v1/apps', options);
        if (response.status !== 200) return exit(`Failed to list apps: ${requestError(response)}`);

        const apps = [];

        for (const app of response.body.apps) {
            const response2 = await createRequest('GET', `/api/v1/apps/${app.id}`, options);
            if (response2.status !== 200) return exit(`Failed to list app: ${requestError(response2)}`);
            response2.body.location = response2.body.location || response2.body.subdomain; // LEGACY support
            apps.push(response2.body);
        }

        const { adminFqdn } = requestOptions(cmd.optsWithGlobals());
        console.log(JSON.stringify({
            apiEndpoint: adminFqdn,
            appStoreOrigin: config.appStoreOrigin(),
            apps: apps
        }, null, 4));
    } catch (error) {
        exit(error);
    }
}

async function restart(localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);
        await restartApp(app, options);
        await waitForHealthy(app.id, options);
        console.log('\n\nApp restarted');
    } catch (error) {
        exit(error);
    }
}

async function start(localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);
        await startApp(app, options);
        await waitForHealthy(app.id, options);
        console.log('\n\nApp started');
    } catch (error) {
        exit(error);
    }
}

async function stop(localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);
        await stopApp(app, options);
        console.log('\n\nApp stopped');
    } catch (error) {
        exit(error);
    }
}

async function backupCreate(localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

        const response = await createRequest('POST', `/api/v1/apps/${app.id}/backup`, options);
        if (response.status !== 202) return exit(`Failed to start backup: ${requestError(response)}`);

        // FIXME: this should be waitForHealthCheck but the box code incorrectly modifies the installationState
        await waitForFinishBackup(app.id, response.body.taskId, options);
        console.log('\n\nApp is backed up');
    } catch (error) {
        exit('\n\nApp backup error: %s', error.message);
    }
}

async function backupList(localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

        const response = await createRequest('GET', `/api/v1/apps/${app.id}/backups`, options);
        if (response.status !== 200) return exit(`Failed to list backups: ${requestError(response)}`);

        if (options.raw) return console.log(JSON.stringify(response.body.backups, null, 4));

        if (response.body.backups.length === 0) return console.log('No backups.');

        response.body.backups = response.body.backups.map(function (backup) {
            backup.creationTime = new Date(backup.creationTime);
            return backup;
        }).sort(function (a, b) { return b.creationTime - a.creationTime; });

        const t = new Table();

        response.body.backups.forEach(function (backup) {
            t.cell('Id', backup.id);
            t.cell('Creation Time', backup.creationTime);
            t.cell('Version', backup.packageVersion);

            t.newRow();
        });

        console.log();
        console.log(t.toString());
    } catch (error) {
        exit(error);
    }
}

async function getLastBackup(app, options) {
    const response = await createRequest('GET', `/api/v1/apps/${app.id}/backups`, options);
    if (response.status !== 200) throw new Error(`Failed to list backups: ${requestError(response)}`);
    if (response.body.backups.length === 0) throw new Error('No backups');

    response.body.backups = response.body.backups.map(function (backup) {
        backup.creationTime = new Date(backup.creationTime);
        return backup;
    }).sort(function (a, b) { return b.creationTime - a.creationTime; });

    return response.body.backups[0].id;
}

async function restore(localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

        let backupId;
        if (!options.backup) {
            backupId = await getLastBackup(app, options);
        } else {
            backupId = options.backup;
        }

        const request = createRequest('POST', `/api/v1/apps/${app.id}/restore`, options);
        const response = await request.send({ backupId });
        if (response.status !== 202) return exit(`Failed to restore app: ${requestError(response)}`);

        // FIXME: this should be waitForHealthCheck but the box code incorrectly modifies the installationState
        await waitForFinishInstallation(app.id, response.body.taskId, options);

        console.log('\n\nApp is restored');

    } catch (error) {
        exit('\n\nApp restore error: %s', error.message);
    }
}

async function importApp(localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

        let data = {};

        if (!options.inPlace) {
            let { remotePath, backupFormat, backupConfig } = options;
            const { backupPath, backupKey } = options;
            let backupFolder;

            if (backupPath) {
                if (backupPath.endsWith('.tar.gz')) {
                    backupFolder = path.dirname(backupPath);
                    remotePath = path.basename(backupPath).replace(/.tar.gz$/, '');
                    backupFormat = 'tgz';
                } else {
                    backupFolder = path.dirname(backupPath);
                    remotePath = path.basename(backupPath);
                    backupFormat = 'rsync';
                }
                backupConfig = { provider: 'filesystem', backupFolder };
                data = { remotePath, backupFormat, backupConfig };
            } else {
                if (!remotePath) return exit('--remote-path, --backup-file or --in-place is required');
                if (!backupFormat) return exit('--backup-format is required');

                if (remotePath.endsWith('.tar.gz')) { // common mistake...
                    return exit('remotePath should not end with .tar.gz. Try again removing the extension from remote path');
                }

                if (backupConfig) {
                    backupConfig = safe.JSON.parse(options.backupConfig);
                    if (!backupConfig) return exit('--backup-config is not a valid JSON');
                } else {
                    backupConfig = null; // ensure null-ness
                }

                data = { remotePath, backupFormat, backupConfig };
            }

            if (backupKey) data.backupConfig.key = backupKey;
        }

        const response = await createRequest('POST', `/api/v1/apps/${app.id}/import`, options);
        if (response.status !== 202) return exit(`Failed to import app: ${requestError(response)}`);

        await waitForFinishInstallation(app.id, response.body.taskId, options);
        console.log('\n\nApp is restored');
    } catch (error) {
        exit('\n\nApp import error: %s', error.message);
    }
}

async function exportApp(localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

        const data = {};

        if (!options.snapshot) return exit('--snapshot is required');

        const request = createRequest('POST', `/api/v1/apps/${app.id}/export`, options);
        const response = await request.send(data);
        if (response.status !== 202) return exit(`Failed to export app: ${requestError(response)}`);

        await waitForFinishInstallation(app.id, response.body.taskId, options);
        console.log('\n\nApp is exported');
    } catch (error) {
        exit('\n\nApp export error: %s', error.message);
    }
}

async function clone(localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

        if (!options.backup) return exit('Use --backup to specify the backup id');

        const location = options.location || await readline.question('Cloned app location: ', {});
        const secondaryDomains = await querySecondaryDomains(app, app.manifest, options);
        const ports = await queryPortBindings(app, app.manifest);
        const backupId = options.backup;

        const domainObject = await selectDomain(location, options);
        const data = {
            backupId,
            location: domainObject.subdomain, // LEGACY
            subdomain: domainObject.subdomain,
            domain: domainObject.domain,
            secondaryDomains,
            ports
        };
        const request = createRequest('POST', `/api/v1/apps/${app.id}/clone`, options);
        const response = await request.send(data);
        if (response.status !== 201) return exit(`Failed to list apps: ${requestError(response)}`);

        // FIXME: this should be waitForHealthCheck but the box code incorrectly modifies the installationState
        console.log('App cloned as id ' + response.body.id);
        await waitForFinishInstallation(response.body.id, response.body.taskId, options);
        console.log('\n\nApp is cloned');
    } catch (error) {
        exit(`\n\nClone error: ${error.message}`);
    }
}

// taken from docker-modem
function demuxStream(stream, stdout, stderr) {
    let header = null;

    stream.on('readable', function() {
        header = header || stream.read(8);
        while (header !== null) {
            const type = header.readUInt8(0);
            const payload = stream.read(header.readUInt32BE(4));
            if (payload === null) break;
            if (type == 2) {
                stderr.write(payload);
            } else {
                stdout.write(payload);
            }
            header = stream.read(8);
        }
    });
}

// cloudron exec - must work interactively. needs tty.
// cloudron exec -- ls asdf  - must work
// cloudron exec -- cat /home/cloudron/start.sh > /tmp/start.sh - must work (test with binary files). should disable tty
// echo "sauce" | cloudron exec -- bash -c "cat - > /app/data/sauce" - test with binary files. should disable tty
// cat ~/tmp/fantome.tar.gz | cloudron exec -- bash -c "tar xvf - -C /tmp" - must show an error
// cat ~/tmp/fantome.tar.gz | cloudron exec -- bash -c "tar zxf - -C /tmp" - must extrack ok
async function exec(args, localOptions, cmd) {
    let stdin = localOptions._stdin || process.stdin; // hack for 'push', 'pull' to reuse this function
    const stdout = localOptions._stdout || process.stdout; // hack for 'push', 'pull' to reuse this function

    const options = cmd.optsWithGlobals();
    let tty = !!options.tty;
    let lang; // used as body param later

    const [error, app] = await safe(getApp(options));
    if (error) return exit(error);
    if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

    if (app.installationState !== 'installed') exit('App is not yet running. Try again later.');

    if (args.length === 0) {
        args = [ '/bin/bash' ];
        tty = true; // override
        lang = 'C.UTF-8';
    }

    if (tty && !stdin.isTTY) exit('stdin is not tty');

    const request = createRequest('POST', `/api/v1/apps/${app.id}/exec`, options);
    const response = await request.send({ cmd: args, tty, lang });
    if (response.status !== 200) return exit(`Failed to create exec: ${requestError(response)}`);
    const execId = response.body.id;

    async function exitWithCode() {
        const response2 = await createRequest('GET', `/api/v1/apps/${app.id}/exec/${execId}`, options);
        if (response2.status !== 200) return exit(`Failed to get exec code: ${requestError(response2)}`);

        process.exit(response2.body.exitCode);
    }

    const { adminFqdn, token, rejectUnauthorized } = requestOptions(cmd.optsWithGlobals());

    const searchParams = new URLSearchParams({
        rows: stdout.rows || 24,
        columns: stdout.columns || 80,
        access_token: token,
        tty
    });

    const req = https.request({
        hostname: adminFqdn,
        path: `/api/v1/apps/${app.id}/exec/${execId}/start?${searchParams.toString()}`,
        method: 'GET',
        headers: {
            'Connection': 'Upgrade',
            'Upgrade': 'tcp'
        },
        rejectUnauthorized
    }, function handler(res) {
        if (res.statusCode === 403) exit('Unauthorized.'); // only admin or only owner (for docker addon)

        exit('Could not upgrade connection to tcp. http status:', res.statusCode);
    });

    req.on('upgrade', function (resThatShouldNotBeUsed, socket /*, upgradeHead*/) {
        // do not use res here! it's all socket from here on
        socket.on('error', exit);

        socket.setNoDelay(true);
        socket.setKeepAlive(true);

        if (tty) {
            stdin.setRawMode(true);
            stdin.pipe(socket, { end: false }); // the remote will close the connection
            socket.pipe(stdout); // in tty mode, stdout/stderr is merged
            socket.on('end', exitWithCode); // server closed the socket
        } else { // create stdin process on demand
            if (typeof stdin === 'function') stdin = stdin();

            stdin.on('data', function (d) {
                var buf = Buffer.alloc(4);
                buf.writeUInt32BE(d.length, 0 /* offset */);
                socket.write(buf);
                socket.write(d);
            });
            stdin.on('end', function () {
                var buf = Buffer.alloc(4);
                buf.writeUInt32BE(0, 0 /* offset */);
                socket.write(buf);
            });

            stdout.on('close', exitWithCode); // this is only emitted when stdout is a file and not the terminal

            demuxStream(socket, stdout, process.stderr); // can get separate streams in non-tty mode
            socket.on('end', function () {  // server closed the socket
                if (typeof stdin.end === 'function') stdin.end(); // required for this process to 'exit' cleanly. do not call exit() because writes may not have finished . the type check is required for when stdin: 'ignore' in execSync, not sure why
                if (stdout !== process.stdout) stdout.end(); // for push stream

                socket.end();

                // process._getActiveHandles(); process._getActiveRequests();
                if (stdout === process.stdout) setImmediate(exitWithCode); // otherwise, we rely on the 'close' event above
            });
        }
    });

    req.on('error', exit); // could not make a request
    req.end(); // this makes the request
}

function push(localDir, remote, localOptions, cmd) {
    // deal with paths prefixed with ~
    const local = localDir.replace(/^~(?=$|\/|\\)/, os.homedir());
    const stat = fs.existsSync(path.resolve(local)) ? fs.lstatSync(local) : null;

    if (stat && stat.isDirectory())  {
        // Create a functor for stdin. If no data event handlers are attached, and there are no stream.pipe() destinations, and the stream is
        // switched into flowing mode, then data will be lost. So, we have to start the tarzip only when exec is ready to attach event handlers.
        localOptions._stdin = function () {
            var tarzip = spawn('tar', ['zcf', '-', '-C', path.dirname(local), path.basename(local)], { stdio: 'pipe' });
            return tarzip.stdout;
        };

        exec(['tar', 'zxvf', '-', '-C', remote], localOptions, cmd);
    } else {
        if (local === '-') {
            localOptions._stdin = process.stdin;
        } else if (stat) {
            localOptions._stdin = fs.createReadStream(local);
        } else {
            exit('local file ' + local + ' does not exist');
        }

        localOptions._stdin.on('error', function (error) { exit('Error pushing', error); });

        if (remote.endsWith('/')) { // dir
            remote = remote + '/' + path.basename(local); // do not use path.join as we want this to be a UNIX path
        }

        exec(['bash', '-c', `cat - > "${remote}"`], localOptions, cmd);
    }
}

function pull(remote, local, localOptions, cmd) {
    if (remote.endsWith('/')) { // dir
        var untar = tar.extract(local); // local directory is created if it doesn't exist!
        var unzip = zlib.createGunzip();

        unzip.pipe(untar);
        localOptions._stdout = unzip;

        exec(['tar', 'zcf', '-', '-C', remote, '.'], localOptions, cmd);
    } else {
        if (fs.existsSync(local) && fs.lstatSync(local).isDirectory()) {
            local = path.join(local, path.basename(remote));
            localOptions._stdout = fs.createWriteStream(local);
        } else if (local === '-') {
            localOptions._stdout = process.stdout;
        } else {
            localOptions._stdout = fs.createWriteStream(local);
        }

        localOptions._stdout.on('error', function (error) { exit('Error pulling', error); });

        exec(['cat', remote], localOptions, cmd);
    }
}

function init(localOptions, cmd) {
    const options = cmd.optsWithGlobals();
    const manifestFilePath = locateManifest();
    if (manifestFilePath && path.dirname(manifestFilePath) === process.cwd()) return exit('CloudronManifest.json already exists in current directory');

    const manifestTemplateFilename = options.appstore ? 'CloudronManifest.appstore.json.ejs' : 'CloudronManifest.json.ejs';

    const manifestTemplate = fs.readFileSync(path.join(__dirname, 'templates/', manifestTemplateFilename), 'utf8');
    const dockerfileTemplate = fs.readFileSync(path.join(__dirname, 'templates/', 'Dockerfile.ejs'), 'utf8');
    const dockerignoreTemplate = fs.readFileSync(path.join(__dirname, 'templates/', 'dockerignore.ejs'), 'utf8');
    const startShTemplate = fs.readFileSync(path.join(__dirname, 'templates/', 'start.sh.ejs'), 'utf8');

    const data = {
        version: '0.1.0',
        title: 'App title',
        httpPort: 3000
    };

    const manifest = ejs.render(manifestTemplate, data);
    fs.writeFileSync('CloudronManifest.json', manifest, 'utf8');

    if (fs.existsSync('Dockerfile')) {
        console.log('Dockerfile already exists, skipping');
    } else {
        const dockerfile = ejs.render(dockerfileTemplate, data);
        fs.writeFileSync('Dockerfile', dockerfile, 'utf8');
    }

    if (fs.existsSync('.dockerignore')) {
        console.log('.dockerignore already exists, skipping');
    } else {
        const dockerignore = ejs.render(dockerignoreTemplate, data);
        fs.writeFileSync('.dockerignore', dockerignore, 'utf8');
    }

    if (fs.existsSync('start.sh')) {
        console.log('start.sh already exists, skipping');
    } else {
        const dockerignore = ejs.render(startShTemplate, {});
        fs.writeFileSync('start.sh', dockerignore, 'utf8');
        fs.chmodSync('start.sh', 0o0775);
    }

    fs.copyFileSync(path.join(__dirname, 'templates/', 'logo.png'), 'logo.png');

    if (options.appstore) {
        if (!fs.existsSync('.gitignore')) fs.writeFileSync('.gitignore', 'node_modules/\n', 'utf8');
        if (!fs.existsSync('DESCRIPTION.md')) fs.writeFileSync('DESCRIPTION.md', '## About\n\nThis app changes everything\n\n', 'utf8');
        if (!fs.existsSync('POSTINSTALL.md')) fs.writeFileSync('POSTINSTALL.md', 'Post installation information\n\n', 'utf8');
        if (!fs.existsSync('CHANGELOG')) fs.writeFileSync('CHANGELOG', '[0.1.0]\n* Initial version\n\n', 'utf8');
    }

    console.log();
    console.log('Now edit the CloudronManifest.json');
    console.log();
}

async function setEnv(app, env, options) {
    assert.strictEqual(typeof app, 'object');
    assert.strictEqual(typeof env, 'object');
    assert.strictEqual(typeof options, 'object');

    const request = createRequest('POST', `/api/v1/apps/${app.id}/configure/env`, options);
    const response = await request.send({ env });
    if (response.status !== 202) return exit(`Failed to set env: ${requestError(response)}`);

    await waitForTask(response.body.taskId, options);
    console.log('\n');
}

async function envSet(envVars, localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

        const response = await createRequest('GET', `/api/v1/apps/${app.id}`, options);
        if (response.status !== 200) return exit(`Failed to get app: ${requestError(response)}`);

        const env = response.body.env;
        envVars.forEach(envVar => {
            const m = envVar.match(/(.*?)=(.*)/);
            if (!m) return exit(`Expecting KEY=VALUE pattern. Got ${envVar}`);
            env[m[1]] = m[2];
        });

        await setEnv(app, env, options);
    } catch (error) {
        exit(error);
    }
}

async function envUnset(envNames, localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

        const response = await createRequest('GET', `/api/v1/apps/${app.id}`, options);
        if (response.status !== 200) return exit(`Failed to get app: ${requestError(response)}`);

        const env = response.body.env;
        envNames.forEach(name => delete env[name]);

        await setEnv(app, env, options);
    } catch (error) {
        exit(error);
    }
}

async function envList(localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

        const response = await createRequest('GET', `/api/v1/apps/${app.id}`, options);
        if (response.status !== 200) return exit(`Failed to get app: ${requestError(response)}`);

        const t = new Table();

        const envNames = Object.keys(response.body.env);
        if (envNames.length === 0) return console.log('No custom environment variables');

        envNames.forEach(function (envName) {
            t.cell('Name', envName);
            t.cell('Value', response.body.env[envName]);

            t.newRow();
        });

        console.log();
        console.log(t.toString());
    } catch (error) {
        exit(error);
    }
}

async function envGet(envName, localOptions, cmd) {
    try {
        const options = cmd.optsWithGlobals();
        const app = await getApp(options);
        if (!app) return exit(NO_APP_FOUND_ERROR_STRING);

        const response = await createRequest('GET', `/api/v1/apps/${app.id}`, options);
        if (response.status !== 200) return exit(`Failed to get app: ${requestError(response)}`);

        console.log(response.body.env[envName]);
    } catch (error) {
        exit(error);
    }
}
