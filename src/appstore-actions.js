/* jshint node:true */

'use strict';

const assert = require('assert'),
    config = require('./config.js'),
    execSync = require('child_process').execSync,
    fs = require('fs'),
    { exit, locateManifest } = require('./helper.js'),
    manifestFormat = require('cloudron-manifestformat'),
    path = require('path'),
    readline = require('./readline.js'),
    safe = require('safetydance'),
    superagent = require('./superagent.js'),
    Table = require('easy-table');

exports = module.exports = {
    login,
    logout,
    info,
    listVersions,
    submit,
    upload,
    revoke,
    approve,
    verifyManifest,

    notify
};

const NO_MANIFEST_FOUND_ERROR_STRING = 'No CloudronManifest.json found';

function requestError(response) {
    if (response.status === 401) return 'Invalid token. Use cloudron appstore login again.';

    return `${response.status} message: ${response.body?.message || response.text || JSON.stringify(response.body)}`; // body is sometimes just a string like in 401
}

function createRequest(method, apiPath, options) {
    const token = options.appstoreToken || config.appStoreToken();

    let url = `${config.appStoreOrigin()}${apiPath}`;
    if (url.includes('?')) url += '&'; else url += '?';
    url += `accessToken=${token}`;
    const request = superagent.request(method, url);
    request.retry(3);
    request.ok(() => true);
    return request;
}

function createUrl(api) {
    return config.appStoreOrigin() + api;
}

// the app argument allows us in the future to get by name or id
async function getAppstoreId(appstoreId) {
    if (appstoreId) return appstoreId.split('@');

    const manifestFilePath = locateManifest();
    if (!manifestFilePath) throw new Error('No CloudronManifest.json found');

    const manifest = safe.JSON.parse(safe.fs.readFileSync(manifestFilePath));
    if (!manifest) throw new Error(`Unable to read manifest ${manifestFilePath}. Error: ${safe.error.message}`);

    return [manifest.id, manifest.version];
}

async function authenticate(options) { // maybe we can use options.token to valid using a profile call?
    if (!options.hideBanner) {
        const webDomain = config.appStoreOrigin().replace('https://api.', '');
        console.log(`${webDomain} login` + ` (If you do not have one, sign up at https://${webDomain}/console.html#/register)`);
    }

    const email = options.email || await readline.question('Email: ', {});
    const password = options.password || await readline.question('Password: ', { noEchoBack: true });

    config.setAppStoreToken(null);

    const response = await superagent.post(createUrl('/api/v1/login')).auth(email, password).send({ totpToken: options.totpToken }).ok(() => true);
    if (response.status === 401 && response.body.message.indexOf('TOTP') !== -1) {
        if (response.body.message === 'TOTP token missing') console.log('A 2FA TOTP Token is required for this account.');

        options.totpToken = await readline.question('2FA token: ', {});
        options.email = email;
        options.password = password;
        options.hideBanner = true;

        return await authenticate(options); // try again with top set
    }

    if (response.status !== 200) {
        console.log('Login failed.');

        options.hideBanner = true;
        options.email = '';
        options.password = '';

        return await authenticate(options);
    }

    config.setAppStoreToken(response.body.accessToken);

    console.log('Login successful.');
}

async function login(localOptions, cmd) {
    const options = cmd.optsWithGlobals();
    await authenticate(options);
}

function logout() {
    config.setAppStoreToken(null);
    console.log('Done.');
}

async function info(localOptions, cmd) {
    const options = cmd.optsWithGlobals();
    const [id, version] = await getAppstoreId(options.appstoreId);

    const response = await createRequest('GET', `/api/v1/developers/apps/${id}/versions/${version}`, options);
    if (response.status !== 200) return exit(new Error(`Failed to list versions: ${requestError(response)}`));

    const manifest = response.body.manifest;
    console.log('id: %s', manifest.id);
    console.log('title: %s', manifest.title);
    console.log('tagline: %s', manifest.tagline);
    console.log('description: %s', manifest.description);
    console.log('website: %s', manifest.website);
    console.log('contactEmail: %s', manifest.contactEmail);
}

async function listVersions(localOptions, cmd) {
    const options = cmd.optsWithGlobals();
    const [id] = await getAppstoreId(options.appstoreId);

    const response = await createRequest('GET', `/api/v1/developers/apps/${id}/versions`, options);
    if (response.status !== 200) return exit(new Error(`Failed to list versions: ${requestError(response)}`));

    if (response.body.versions.length === 0) return console.log('No versions found.');

    if (options.raw) return console.log(JSON.stringify(response.body.versions, null, 2));

    const versions = response.body.versions.reverse();
    const t = new Table();

    versions.forEach(function (version) {
        t.cell('Version', version.manifest.version);
        t.cell('Creation Date', version.creationDate);
        t.cell('Image', version.manifest.dockerImage);
        t.cell('Publish state', version.publishState);
        t.newRow();
    });

    console.log();
    console.log(t.toString());
}

function parseChangelog(file, version) {
    let changelog = '';
    const data = safe.fs.readFileSync(file, 'utf8');
    if (!data) return null;
    const lines = data.split('\n');

    version = version.replace(/-.*/, ''); // remove any prerelease

    let i;
    for (i = 0; i < lines.length; i++) {
        if (lines[i] === '[' + version + ']') break;
    }

    for (i = i + 1; i < lines.length; i++) {
        if (lines[i] === '') continue;
        if (lines[i][0] === '[') break;

        changelog += lines[i] + '\n';
    }

    return changelog;
}

async function addVersion(manifest, baseDir, options) {
    assert.strictEqual(typeof manifest, 'object');
    assert.strictEqual(typeof baseDir, 'string');
    assert.strictEqual(typeof options, 'object');

    let iconFilePath = null;
    if (manifest.icon) {
        let iconFile = manifest.icon; // backward compat
        if (iconFile.slice(0, 7) === 'file://') iconFile = iconFile.slice(7);

        iconFilePath = path.isAbsolute(iconFile) ? iconFile : path.join(baseDir, iconFile);
        if (!fs.existsSync(iconFilePath)) throw new Error('icon not found at ' + iconFilePath);
    }

    if (manifest.description.slice(0, 7) === 'file://') {
        let descriptionFilePath = manifest.description.slice(7);
        descriptionFilePath = path.isAbsolute(descriptionFilePath) ? descriptionFilePath : path.join(baseDir, descriptionFilePath);
        manifest.description = safe.fs.readFileSync(descriptionFilePath, 'utf8');
        if (!manifest.description && safe.error) throw(new Error('Could not read/parse description ' + safe.error.message));
        if (!manifest.description) throw new Error('Description cannot be empty');
    }

    if (manifest.postInstallMessage && manifest.postInstallMessage.slice(0, 7) === 'file://') {
        let postInstallFilePath = manifest.postInstallMessage.slice(7);
        postInstallFilePath = path.isAbsolute(postInstallFilePath) ? postInstallFilePath : path.join(baseDir, postInstallFilePath);
        manifest.postInstallMessage = safe.fs.readFileSync(postInstallFilePath, 'utf8');
        if (!manifest.postInstallMessage && safe.error) throw(new Error('Could not read/parse postInstall ' + safe.error.message));
        if (!manifest.postInstallMessage) throw new Error('PostInstall file specified but it is empty');
    }

    if (manifest.changelog.slice(0, 7) === 'file://') {
        let changelogPath = manifest.changelog.slice(7);
        changelogPath = path.isAbsolute(changelogPath) ? changelogPath : path.join(baseDir, changelogPath);
        manifest.changelog = parseChangelog(changelogPath, manifest.version);
        if (!manifest.changelog) throw new Error('Bad changelog format or missing changelog for this version');
    }
    const request = createRequest('POST', `/api/v1/developers/apps/${manifest.id}/versions`, options);
    if (iconFilePath) request.attach('icon', iconFilePath);
    request.attach('manifest', Buffer.from(JSON.stringify(manifest)));
    const response = await request;
    if (response.status === 409) throw new Error('This version already exists. Use --force to overwrite.');
    if (response.status !== 204) throw new Error(`Failed to publish version: ${requestError(response)}`);
}

async function updateVersion(manifest, baseDir, options) {
    assert.strictEqual(typeof manifest, 'object');
    assert.strictEqual(typeof baseDir, 'string');
    assert.strictEqual(typeof options, 'object');

    let iconFilePath = null;
    if (manifest.icon) {
        let iconFile = manifest.icon; // backward compat
        if (iconFile.slice(0, 7) === 'file://') iconFile = iconFile.slice(7);

        iconFilePath = path.isAbsolute(iconFile) ? iconFile : path.join(baseDir, iconFile);
        if (!fs.existsSync(iconFilePath)) throw new Error('icon not found at ' + iconFilePath);
    }

    if (manifest.description.slice(0, 7) === 'file://') {
        let descriptionFilePath = manifest.description.slice(7);
        descriptionFilePath = path.isAbsolute(descriptionFilePath) ? descriptionFilePath : path.join(baseDir, descriptionFilePath);
        manifest.description = safe.fs.readFileSync(descriptionFilePath, 'utf8');
        if (!manifest.description) throw new Error('Could not read description ' + safe.error.message);
    }

    if (manifest.postInstallMessage && manifest.postInstallMessage.slice(0, 7) === 'file://') {
        let postInstallFilePath = manifest.postInstallMessage.slice(7);
        postInstallFilePath = path.isAbsolute(postInstallFilePath) ? postInstallFilePath : path.join(baseDir, postInstallFilePath);
        manifest.postInstallMessage = safe.fs.readFileSync(postInstallFilePath, 'utf8');
        if (!manifest.postInstallMessage) throw new Error('Could not read/parse postInstall ' + safe.error.message);
    }

    if (manifest.changelog.slice(0, 7) === 'file://') {
        let changelogPath = manifest.changelog.slice(7);
        changelogPath = path.isAbsolute(changelogPath) ? changelogPath : path.join(baseDir, changelogPath);
        manifest.changelog = parseChangelog(changelogPath, manifest.version);
        if (!manifest.changelog) throw new Error('Could not read changelog or missing version changes');
    }

    const request = createRequest('PUT', `/api/v1/developers/apps/${manifest.id}/versions/${manifest.version}`, options);
    if (iconFilePath) request.attach('icon', iconFilePath);
    request.attach('manifest', Buffer.from(JSON.stringify(manifest)));
    const response = await request;
    if (response.status !== 204) throw new Error(`Failed to publish version: ${requestError(response)}`);
}

async function verifyManifest(localOptions, cmd) {
    const options = cmd.optsWithGlobals();
    // try to find the manifest of this project
    const manifestFilePath = locateManifest();
    if (!manifestFilePath) return exit(NO_MANIFEST_FOUND_ERROR_STRING);

    const result = manifestFormat.parseFile(manifestFilePath);
    if (result.error) return exit(result.error.message);

    const manifest = result.manifest;

    const sourceDir = path.dirname(manifestFilePath);
    const appConfig = config.getAppConfig(sourceDir);

    // image can be passed in options for buildbot
    if (options.image) {
        manifest.dockerImage = options.image;
    } else {
        manifest.dockerImage = appConfig.dockerImage;
    }

    if (!manifest.dockerImage) exit('No docker image found, run `cloudron build` first');

    // ensure we remove the docker hub handle
    if (manifest.dockerImage.indexOf('docker.io/') === 0) manifest.dockerImage = manifest.dockerImage.slice('docker.io/'.length);

    const error = manifestFormat.checkAppstoreRequirements(manifest);
    if (error) return exit(error);
}

async function upload(localOptions, cmd) {
    const options = cmd.optsWithGlobals();
    // try to find the manifest of this project
    const manifestFilePath = locateManifest();
    if (!manifestFilePath) return exit(NO_MANIFEST_FOUND_ERROR_STRING);

    const result = manifestFormat.parseFile(manifestFilePath);
    if (result.error) return exit(result.error.message);

    const manifest = result.manifest;

    const sourceDir = path.dirname(manifestFilePath);
    const appConfig = config.getAppConfig(sourceDir);

    // image can be passed in options for buildbot
    if (options.image) {
        manifest.dockerImage = options.image;
    } else {
        const gitCommit = execSync('git rev-parse HEAD', { encoding: 'utf8' }).trim();
        if (appConfig.gitCommit !== gitCommit) {
            console.log(`This build ${appConfig.dockerImage} was made from git hash ${appConfig.gitCommit} but you are now at ${gitCommit}`);
            if (!appConfig.gitCommit) return exit('The build is stale');
            const output = execSync(`git diff ${appConfig.gitCommit}..HEAD --name-only`, { encoding: 'utf8'});
            const changedFiles = output.trim().split('\n').filter(filepath => !filepath.match(/(^CHANGELOG|README|CloudronManifest|test\|.git|screenshots|renovate|LICENSE|POSTINSTALL|.docker|logo)/));
            if (changedFiles.length) return exit(`The build is stale. Changed files: ${changedFiles.join(',')}`);
        }
        manifest.dockerImage = appConfig.dockerImage;
    }

    if (!manifest.dockerImage) exit('No docker image found, run `cloudron build` first');

    // ensure we remove the docker hub handle
    if (manifest.dockerImage.indexOf('docker.io/') === 0) manifest.dockerImage = manifest.dockerImage.slice('docker.io/'.length);

    const error = manifestFormat.checkAppstoreRequirements(manifest);
    if (error) return exit(error);

    const [repo, tag] = manifest.dockerImage.split(':');
    const [tagError, tagResponse] = await safe(superagent.get(`https://hub.docker.com/v2/repositories/${repo}/tags/${tag}`).ok(() => true));
    if (tagError || tagResponse.status !== 200) return exit(`Failed to find docker image in dockerhub. check https://hub.docker.com/r/${repo}/tags : ${tagError || requestError(tagResponse)}`);

    // ensure the app is known on the appstore side
    const baseDir = path.dirname(manifestFilePath);

    const request = createRequest('POST', '/api/v1/developers/apps', options);
    request.send({ id: manifest.id });
    const response = await request;
    if (response.status !== 409 && response.status !== 201) return exit(`Failed to add app: ${requestError(response)}`); // 409 means already exists
    console.log(`Uploading ${manifest.id}@${manifest.version} (dockerImage: ${manifest.dockerImage}) for testing`);

    const [error2] = await safe(options.force ? updateVersion(manifest, baseDir, options) : addVersion(manifest, baseDir, options));
    if (error2) return exit(error2);
}

async function submit(localOptions, cmd) {
    const options = cmd.optsWithGlobals();

    // try to find the manifest of this project
    const manifestFilePath = locateManifest();
    if (!manifestFilePath) return exit(NO_MANIFEST_FOUND_ERROR_STRING);

    const result = manifestFormat.parseFile(manifestFilePath);
    if (result.error) return exit(result.error.message);

    const manifest = result.manifest;

    const response = await createRequest('POST', `/api/v1/developers/apps/${manifest.id}/versions/${manifest.version}/submit`, options);
    if (response.status === 404) {
        console.log(`No version ${manifest.version} found. Please use 'cloudron apsptore upload' first`);
        return exit('Failed to submit app for review.');
    }

    if (response.status !== 200) return exit(`Failed to submit app: ${requestError(response)}`);

    console.log('App submitted for review.');
    console.log('You will receive an email when approved.');
}

async function revoke(localOptions, cmd) {
    const options = cmd.optsWithGlobals();
    const [id, version] = await getAppstoreId(options.appstoreId);
    if (!version) return exit('--appstore-id must be of the format id@version');

    console.log(`Revoking ${id}@${version}`);

    const response = await createRequest('POST', `/api/v1/developers/apps/${id}/versions/${version}/revoke`, options);
    if (response.status !== 200) return exit(`Failed to revoke version: ${requestError(response)}`);

    console.log('version revoked.');
}

async function approve(localOptions, cmd) {
    const options = cmd.optsWithGlobals();

    const [appstoreId, version] = await getAppstoreId(options.appstoreId);

    if (!version) return exit('--appstore-id must be of the format id@version');

    console.log(`Approving ${appstoreId}@${version}`);

    let defaultBranch, latestTag;
    if (options.gitPush) {
        // Git repo pre-flight checks: checking if latest tag matches latest commit
        defaultBranch = safe.child_process.execSync(`git symbolic-ref refs/remotes/origin/HEAD | sed 's@^refs/remotes/origin/@@'`, { encoding: 'utf8' });
        if (safe.error) return exit(`Failed to get default branch: ${safe.error.message}`);
        defaultBranch = defaultBranch.trim();

        let defaultBranchSha = safe.child_process.execSync(`git rev-parse ${defaultBranch}`, { encoding: 'utf8' });
        if (safe.error) return exit(`Failed to get head of ${defaultBranch}: ${safe.error.message}`);
        defaultBranchSha = defaultBranchSha.trim();

        latestTag = safe.child_process.execSync('git describe --tags --abbrev=0', { encoding: 'utf8' });
        if (safe.error) return exit(`Failed to get latest tag: ${safe.error.message}`);
        latestTag = latestTag.trim();

        let latestTagSha = safe.child_process.execSync(`git rev-list -n 1 ${latestTag}`, { encoding: 'utf8' });
        if (safe.error) return exit(`Failed to get head of ${defaultBranch}: ${safe.error.message}`);
        latestTagSha = latestTagSha.trim();

        if (defaultBranchSha !== latestTagSha) console.warn(`Latest tag ${latestTag} does not match HEAD of ${defaultBranch}`);
    }

    const response = await createRequest('POST', `/api/v1/developers/apps/${appstoreId}/versions/${version}/approve`, options);
    if (response.status !== 200) return exit(`Failed to approve version: ${requestError(response)}`);

    if (options.gitPush) {
        safe.child_process.execSync(`git push --atomic origin ${defaultBranch} ${latestTag}`, { encoding: 'utf8' });
        if (safe.error) return exit(`Failed to get last release tag: ${safe.error.message}`);
    }

    console.log('Approved.');
    console.log('');

    const response2 = await createRequest('GET', `/api/v1/developers/apps/${appstoreId}/versions/${version}`, options);
    if (response2.status !== 200) return exit(`Failed to list apps: ${requestError(response)}`);

    console.log('Changelog for forum update: ' + response2.body.manifest.forumUrl);
    console.log('');
    console.log('[' + version + ']');
    console.log(response2.body.manifest.changelog);
    console.log('');
}

// https://docs.nodebb.org/api/read/
// https://docs.nodebb.org/api/write/
async function notify() {
    if (!process.env.NODEBB_API_TOKEN) return exit('NODEBB_API_TOKEN env var has to be set');
    const apiToken = process.env.NODEBB_API_TOKEN;

    const manifestFilePath = locateManifest();
    if (!manifestFilePath) return exit('Could not locate CloudronManifest.json');

    const result = manifestFormat.parseFile(manifestFilePath);
    if (result.error) return exit(new Error(`Invalid CloudronManifest.json: ${result.error.message}`));
    const { manifest } = result;

    let postContent = null;
    if (manifest.changelog.slice(0, 7) === 'file://') {
        const baseDir = path.dirname(manifestFilePath);
        let changelogPath = manifest.changelog.slice(7);
        changelogPath = path.isAbsolute(changelogPath) ? changelogPath : path.join(baseDir, changelogPath);
        const changelog = parseChangelog(changelogPath, manifest.version);
        if (!changelog) return exit('Bad changelog format or missing changelog for this version');
        postContent = `[${manifest.version}]\n${changelog}\n`;
    } else {
        postContent = `[${manifest.version}]\n${manifest.changelog}\n`;
    }

    if (!manifest.forumUrl) return exit(new Error('CloudronManifest.json does not have a forumUrl'));
    const categoryMatch = manifest.forumUrl.match(/category\/(.*)\//);
    if (!categoryMatch) return exit('Unable to detect category id');
    const categoryId = categoryMatch[1];

    const categoryResponse = await superagent.get(`https://forum.cloudron.io/api/v3/categories/${categoryId}/topics`).set('Authorization', `Bearer ${apiToken}`).ok(() => true);
    if (categoryResponse.status !== 200) return exit(`Unable to get topics of category: ${requestError(categoryResponse)}`);
    const topic = categoryResponse.body.response.topics.find(t => t.title.includes('Package Updates'));
    if (!topic) return exit('Could not find the Package Update topic');
    const topicId = topic.tid;

    const pageCountResponse = await superagent.get(`https://forum.cloudron.io/api/topic/pagination/${topicId}`).set('Authorization', `Bearer ${apiToken}`).ok(() => true);
    if (pageCountResponse.status !== 200) return exit(`Unable to get page count of topic: ${requestError(pageCountResponse)}`);
    const pageCount = pageCountResponse.body.pagination.pageCount;

    for (let page = 1; page <= pageCount; page++) {
        const pageResponse = await superagent.get(`https://forum.cloudron.io/api/topic/${topicId}?page=${page}`).set('Authorization', `Bearer ${apiToken}`).ok(() => true);
        if (pageResponse.status !== 200) return exit(`Unable to get topics of category: ${requestError(pageResponse)}`);
        for (const post of pageResponse.body.posts) { // post.content is html!
            if (post.content.includes(`[${manifest.version}]`)) return exit(`Version ${manifest.version} is already on the forum.\n${post.content}`);
        }
    }

    // https://docs.nodebb.org/api/write/#tag/topics/paths/~1topics~1%7Btid%7D/post
    const postData = {
        content: postContent,
        toPid: 0 // which post is this post a reply to
    };
    const postResponse = await superagent.post(`https://forum.cloudron.io/api/v3/topics/${topicId}`).set('Authorization', `Bearer ${apiToken}`).send(postData).ok(() => true);
    if (postResponse.status !== 200) return exit(`Unable to create changelog post: ${requestError(postResponse)}`);
    console.log('Posted to forum');
}
