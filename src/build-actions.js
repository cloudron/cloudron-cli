'use strict';

exports = module.exports = {
    login,
    build,
    logs,
    status,
    push
};

const assert = require('assert'),
    config = require('./config.js'),
    crypto = require('crypto'),
    { EventSource } = require('eventsource'),
    execSync = require('child_process').execSync,
    exit = require('./helper.js').exit,
    fs = require('fs'),
    helper = require('./helper.js'),
    manifestFormat = require('cloudron-manifestformat'),
    micromatch = require('micromatch'),
    os = require('os'),
    path = require('path'),
    readline = require('./readline.js'),
    safe = require('safetydance'),
    stream = require('stream/promises'),
    superagent = require('./superagent.js'),
    tar = require('tar-fs'),
    url = require('url');

function requestError(response) {
    if (response.status === 401 || response.status === 403) return 'Invalid token. Use cloudron build login again.';

    return `${response.status} message: ${response.body?.message || response.text || null}`;
}

// analyzes options and merges with any existing build service config
async function resolveBuildServiceConfig(options) {
    const buildService = config.getBuildServiceConfig();
    if (!buildService.type) buildService.type = 'local'; // default

    if (options.local) {
        buildService.type = 'local';
    } else if (options.setBuildService) { // stash for future use
        buildService.token = null;
        buildService.url = null;
        buildService.type = 'remote';

        let url;
        if (typeof options.setBuildService === 'string') {
            url = options.setBuildService;
        } else {
            url = await readline.question('Enter build service URL: ', { });
        }

        if (url.indexOf('://') === -1) url = `https://${url}`;
        buildService.url = url;
    }

    if (options.buildServiceToken) buildService.token = options.buildServiceToken;

    config.setBuildServiceConfig(buildService);

    return buildService;
}

async function login(localOptions, cmd) {
    const options = cmd.optsWithGlobals();

    const buildServiceConfig = await resolveBuildServiceConfig(options);

    console.log('Build Service login' + ` (${buildServiceConfig.url}):`);

    const token = options.buildServiceToken || await readline.question('Token: ', {});

    const response = await superagent.get(`${buildServiceConfig.url}/api/v1/profile`).query({ accessToken: token }).ok(() => true);
    if (response.status === 401 || response.status === 403) return exit(`Authentication error: ${requestError(response)}`);
    if (response.status !== 200) return exit(`Unexpected response: ${requestError(response)}`);

    buildServiceConfig.token = token;
    config.setBuildServiceConfig(buildServiceConfig);

    console.log('Login successful.');
}

async function followBuildLog(buildId, raw) {
    assert.strictEqual(typeof buildId, 'string');
    assert.strictEqual(typeof raw, 'boolean');

    // EventSource always requires http
    let tmp = url.parse(config.getBuildServiceConfig().url);
    if (tmp.protocol !== 'https:' && tmp.protocol !== 'http:') tmp = url.parse('http://' + config.getBuildServiceConfig().url);

    const es = new EventSource(`${tmp.href}api/v1/builds/${buildId}/logstream?accessToken=${config.getBuildServiceConfig().token}`);
    let prevId = null, prevWasStatus = false;

    es.addEventListener('message', function (e) {
        if (raw) return console.dir(e);

        const data = safe.JSON.parse(e.data);
        if (!data) return; // this is a bug in docker or our build server

        if (data.status) { // image push log
            if (data.id && data.id === prevId) {
                if (process.stdout.isTTY) {
                    // the code below does not work os x if the line wraps, maybe we should clip the text to window size?
                    process.stdout.clearLine();
                    process.stdout.cursorTo(0);
                }
            } else if (prevWasStatus) {
                process.stdout.write('\n');
            }

            process.stdout.write(data.status + (data.id ? ' ' + data.id : '') + (data.progress ? ' ' + data.progress : ''));

            prevId = data.id;
            prevWasStatus = true;

            return;
        }

        if (prevWasStatus === true) {
            process.stdout.write('\n');
            prevId = null;
            prevWasStatus = false;
        }

        if (data.stream) { // build log
            process.stdout.write(data.stream);
        } else if (data.message) {
            console.log(data.message);
        } else if (data.errorDetail) {
            console.log(data.errorDetail.message ? data.errorDetail.message : data.errorDetail);
        } else if (typeof data.error === 'string') {
            console.log(data.error);
        } else if (data.error) {
            console.error(data.error);
        }
    });

    let didConnect = false;
    es.addEventListener('open', () => didConnect = true, { once: true });

    return new Promise((resolve, reject) => {
        es.addEventListener('error', function (error) { // server close or network error or some interruption
            if (raw) console.dir(error);

            es.close();
            if (didConnect) resolve(); else reject(new Error('Failed to connect'));
        }, { once: true });
    });
}

async function getStatus(buildId) {
    const buildServiceConfig = config.getBuildServiceConfig();

    const response2 = await superagent.get(`${buildServiceConfig.url}/api/v1/builds/${buildId}`)
        .query({ accessToken: buildServiceConfig.token })
        .ok(() => true);
    if (response2.status !== 200) throw new Error(`Failed to get status: ${requestError(response2)}`);
    return response2.body.status;
}

function dockerignoreMatcher(dockerignorePath) {
    let patterns = [];

    if (fs.existsSync(dockerignorePath)) {
        patterns = fs.readFileSync(dockerignorePath, 'utf8').split('\n');

        patterns = patterns.filter(function (line) { return line[0] !== '#'; });
        patterns = patterns.map(function (line) {
            let l = line.trim();

            while (l[0] === '/') l = l.slice(1);
            while (l[l.length-1] === '/') l = l.slice(0, -1);

            return l;
        });
        patterns = patterns.filter(function (line) { return line.length !== 0; });
    }

    return function ignore(path) {
        return micromatch([ path ], patterns, { dot: true }).length == 1;
    };
}

async function buildLocal(manifest, sourceDir, appConfig, options) {
    let tag;
    if (options.tag) {
        tag = options.tag;
    } else {
        // use a simple timestamp as tag. tried to use manifest.version here but this requires the user to remember to
        // bump the manifest version before building (and for approval for us). maybe another idea is to just use a constant 'dev' tag
        const random = crypto.randomBytes(24 / 8).toString('hex');
        tag = new Date().toISOString().replace(/[-:Z]/g, '').replace(/T|\./g, '-') + random;
    }

    const dockerImage = `${appConfig.repository}:${tag}`;

    console.log('Building locally as %s', dockerImage);
    console.log();

    const buildArgsCmdLine = options.buildArg.map(function (a) { return `--build-arg "${a}"`; }).join(' ');

    let dockerfile = 'Dockerfile';
    if (options.file) dockerfile = options.file;
    else if (fs.existsSync(`${sourceDir}/Dockerfile.cloudron`)) dockerfile = 'Dockerfile.cloudron';
    else if (fs.existsSync(`${sourceDir}/cloudron/Dockerfile`)) dockerfile = 'cloudron/Dockerfile';
    execSync(`docker build ${!options.cache ? '--no-cache' : ''} -t ${dockerImage} -f ${dockerfile} ${buildArgsCmdLine} ${sourceDir}`, { stdio: 'inherit' });

    if (options.push) {
        console.log();
        console.log(`Pushing ${dockerImage}`);

        safe.child_process.execSync(`docker push ${dockerImage}`, { stdio: 'inherit' });
        if (safe.error) exit('Failed to push image (are you logged in? if not, use "docker login")');
    }

    const result = safe.child_process.execSync(`docker inspect --format="{{index .RepoDigests 0}}" ${dockerImage}`, { encoding: 'utf8' });
    if (safe.error) exit('Failed to inspect image');
    const match = /.*@sha256:(.*)/.exec(result.trim());
    if (!match) exit('Failed to detect sha256');

    appConfig.dockerImage = `${dockerImage}`;
    appConfig.dockerImageSha256 = match[1]; // stash this separately for now
    config.setAppConfig(sourceDir, appConfig);
}

async function buildRemote(manifest, sourceDir, appConfig, options) {
    console.log('Using build service', config.getBuildServiceConfig().url);

    let tag;
    if (options.tag) {
        tag = options.tag;
    } else {
        // use a simple timestamp as tag. tried to use manifest.version here but this requires the user to remember to
        // bump the manifest version before building (and for approval for us). maybe another idea is to just use a constant 'dev' tag
        const random = crypto.randomBytes(24 / 8).toString('hex');
        tag = new Date().toISOString().replace(/[-:Z]/g, '').replace(/T|\./g, '-') + random;
    }

    const dockerImage = `${appConfig.repository}:${tag}`;

    console.log('Building %s', dockerImage);

    const sourceArchiveFilePath = path.join(os.tmpdir(), path.basename(sourceDir) + '.tar.gz');
    const dockerignoreFilePath = path.join(sourceDir, '.dockerignore');
    const ignoreMatcher = dockerignoreMatcher(dockerignoreFilePath);

    console.log('Uploading source tarball...');

    const tarStream = tar.pack(sourceDir, {
        ignore: function (name) {
            return ignoreMatcher(name.slice(sourceDir.length + 1)); // make name as relative path
        }
    });
    const sourceArchiveStream = fs.createWriteStream(sourceArchiveFilePath);

    const [tarError] = await safe(stream.pipeline(tarStream, sourceArchiveStream));
    if (tarError) return exit(`Could not tar: ${tarError.message}`);

    let dockerfile = 'Dockerfile';
    if (options.file) dockerfile = options.file;
    else if (fs.existsSync(`${sourceDir}/Dockerfile.cloudron`)) dockerfile = 'Dockerfile.cloudron';
    else if (fs.existsSync(`${sourceDir}/cloudron/Dockerfile`)) dockerfile = 'cloudron/Dockerfile';

    const buildArgsObject = {};
    options.buildArg.forEach(function (a) {
        const key = a.slice(0, a.indexOf('='));
        const value = a.slice(a.indexOf('=')+1);
        buildArgsObject[key] = value;
    });

    const buildServiceConfig = config.getBuildServiceConfig();
    const response = await superagent.post(`${buildServiceConfig.url}/api/v1/builds`)
        .query({ accessToken: buildServiceConfig.token, noCache: !options.cache, dockerfile: dockerfile, noPush: !options.push })
        .field('dockerImageRepo', appConfig.repository)
        .field('dockerImageTag', tag)
        .field('buildArgs', JSON.stringify(buildArgsObject))
        .attach('sourceArchive', sourceArchiveFilePath)
        .ok(() => true);
    if (response.status === 413) return exit('Failed to build app. The app source is too large.\nPlease adjust your .dockerignore file to only include neccessary files.');
    if (response.status !== 201) return exit(`Failed to upload app for building: ${requestError(response)}`);

    const buildId = response.body.id;
    console.log(`BuildId: ${buildId}`);

    const [logsError] = await safe(followBuildLog(buildId, !!options.raw));
    if (logsError) console.log(`Failed to get logs: ${logsError.message}`);

    const [statusError, status] = await safe(getStatus(buildId));
    if (statusError) return exit(`Failed to get status: ${statusError.message}`);
    if (status !== 'success') return exit('Failed to build app. See log output above.');

    appConfig.dockerImage = dockerImage;
    // appConfig.dockerImageSha256 = match[1]; // stash this separately for now
    config.setAppConfig(sourceDir, appConfig);

    console.log(`Docker image: ${dockerImage}`);
    console.log('\nBuild successful');

    exit();
}

async function build(localOptions, cmd) {
    const options = cmd.optsWithGlobals();

    // try to find the manifest of this project
    const manifestFilePath = helper.locateManifest();
    if (!manifestFilePath) return exit('No CloudronManifest.json found');

    const result = manifestFormat.parseFile(manifestFilePath);
    if (result.error) return exit('Error in CloudronManifest.json: ' + result.error.message);

    const manifest = result.manifest;
    const sourceDir = path.dirname(manifestFilePath);

    const appConfig = config.getAppConfig(sourceDir);
    const buildServiceConfig = await resolveBuildServiceConfig(options);

    let repository = appConfig.repository;
    if (!repository || options.setRepository) {
        if (typeof options.setRepository === 'string') {
            repository = options.setRepository;
        } else {
            repository = await readline.question(`Enter docker repository (e.g registry/username/${manifest.id || path.basename(sourceDir)}): `, {});
            if (!repository) exit('No repository provided');
            console.log();
        }

        appConfig.repository = repository;
        config.setAppConfig(sourceDir, appConfig);
    }

    appConfig.gitCommit = execSync('git rev-parse HEAD', { encoding: 'utf8' }).trim(); // when the build gets saved, save the gitCommit also
    if (buildServiceConfig.type === 'local') {
        await buildLocal(manifest, sourceDir, appConfig, options);
    } else if (buildServiceConfig.type === 'remote' && buildServiceConfig.url) {
        await buildRemote(manifest, sourceDir, appConfig, options);
    } else {
        exit('Unknown build service type or missing build service url. Rerun with --reset-build-service');
    }
}

async function logs(localOptions, cmd) {
    const options = cmd.optsWithGlobals();

    if (!options.id) return exit('buildId is required');

    const [logsError] = await safe(followBuildLog(options.id, !!options.raw));
    if (logsError) console.log(`Failed to get logs: ${logsError.message}`);
}

async function status(localOptions, cmd) {
    const options = cmd.optsWithGlobals();

    if (!options.id) return exit('buildId is required');

    const [statusError, status] = await safe(getStatus(options.id));
    if (statusError) return exit(`Failed to get status: ${statusError.message}`);
    console.log(status);
}

async function push(localOptions, cmd) {
    const options = cmd.optsWithGlobals();

    if (!options.id) return exit('buildId is required');
    let repository, tag;
    if (options.image) {
        [repository, tag] = options.image.split(':');
    } else {
        if (!options.repository) return exit('repository is required');
        if (!options.tag) return exit('tag is required');
        repository = options.repository;
        tag = options.tag;
    }

    const buildServiceConfig = await resolveBuildServiceConfig(options);

    const response = await superagent.post(`${buildServiceConfig.url}/api/v1/builds/${options.id}/push`)
        .query({ accessToken: buildServiceConfig.token })
        .send({ dockerImageRepo: repository, dockerImageTag: tag })
        .ok(() => true);
    if (response.status !== 201) return exit(`Failed to push: ${requestError(response)}`);

    const [logsError] = await safe(followBuildLog(options.id, !!options.raw));
    if (logsError) console.log(`Failed to get logs: ${logsError.message}`);

    const [statusError, status] = await safe(getStatus(options.id));
    if (statusError) return exit(`Failed to get status: ${statusError.message}`);
    if (status !== 'success') return exit('Failed to push app. See log output above.');
}
