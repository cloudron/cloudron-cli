/* jshint node:true */

'use strict';

const helper = require('./helper.js'),
    util = require('util');

exports = module.exports = function (options, cmd) {
    var completion = '';

    const commands = [];
    for (const command of cmd.parent.commands) {
        if (command._name === '*' || command._name === 'completion') continue;
        if (command._name) commands.push(command._name);
        if (command._alias) commands.push(command._alias);
    }
    commands.sort();

    if (!process.env.SHELL) helper.exit('Unable to detect shell');
    if (process.env.SHELL.indexOf('zsh') !== -1) {
        completion += '\n';
        completion += 'function $$cloudron_completion() {\n';
        completion += '  compls="' + commands.join(' ') + '"\n';
        completion += '  completions=(${=compls})\n';
        completion += '  compadd -- $completions\n';
        completion += '}\n';
        completion += '\n';
        completion += 'compdef $$cloudron_completion cloudron\n';
    } else if (process.env.SHELL.indexOf('bash') !== -1) {
        completion += '\n';
        completion += '_cloudron()\n';
        completion += '{\n';
        completion += '  COMPREPLY=( $( compgen -W "' + commands.join(' ') + '" -- ${COMP_WORDS[COMP_CWORD]} ) )\n';
        completion += '}\n';
        completion += 'complete -o default -o nospace -F _cloudron  cloudron\n';

    } else {
        helper.exit(util.format('Unsupported shell %s', process.env.SHELL));
    }

    console.log(completion);
};
