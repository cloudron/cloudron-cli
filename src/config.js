/* jshint node:true */

'use strict';

var fs = require('fs'),
    path = require('path'),
    safe = require('safetydance');

var HOME = process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE;

exports = module.exports = {
    setActive: setActive,

    set: set,
    get: get,

    cloudronGet: cloudronGet,
    cloudronSet: cloudronSet,

    // appstore
    appStoreToken,
    setAppStoreToken,
    appStoreOrigin,

    // per app
    getAppConfig: (path) => get(['apps', path]) || {},
    setAppConfig: (path, c) => set(['apps', path], c),

    // build service
    getBuildServiceConfig: () => get('buildService') || {},
    setBuildServiceConfig: (c) => set('buildService', c),

    // current cloudron
    token: cloudronGet.bind(null, 'token'),
    setToken: cloudronSet.bind(null, 'token'),

    cloudron: cloudronGet.bind(null, 'cloudron'),
    setCloudron: cloudronSet.bind(null, 'cloudron'),

    apiEndpoint: cloudronGet.bind(null, 'apiEndpoint'),
    setApiEndpoint: cloudronSet.bind(null, 'apiEndpoint'),

    allowSelfsigned: cloudronGet.bind(null, 'allowSelfsigned'),
    setAllowSelfsigned: cloudronSet.bind(null, 'allowSelfsigned'),

    // for testing
    _configFilePath: path.join(HOME, '.cloudron.json')
};

var gConfig = safe.JSON.parse(safe.fs.readFileSync(exports._configFilePath)) || {};

// per default we fetch the last cloudron which a user performed an explicit login
var gCurrent = safe.query(gConfig, 'cloudrons.default') || null;

// This is mostly called from helper.detectCloudronApiEndpoint() which will select the correct config section then
function setActive(apiEndpoint) {
    gCurrent = apiEndpoint;
}

function save() {
    fs.writeFileSync(exports._configFilePath, JSON.stringify(gConfig, null, 4));
}

function set(key, value) {
    // reload config from disk and set. A parallel cli process could have adjusted values
    gConfig = safe.JSON.parse(safe.fs.readFileSync(exports._configFilePath)) || {};

    safe.set(gConfig, key, value);
    save();
}

function get(key) {
    return safe.query(gConfig, key);
}

function cloudronGet(key) {
    return get(['cloudrons', gCurrent, key]);
}

function cloudronSet(key, value) {
    return set(['cloudrons', gCurrent, key], value);
}

function appStoreOrigin() {
    return process.env.APPSTORE_ORIGIN || get('appStoreOrigin') || 'https://api.cloudron.io';
}

function appStoreToken() {
    return get(['appStore', appStoreOrigin().replace('https://', ''), 'token']);
}

function setAppStoreToken(token) {
    set(['appStore', appStoreOrigin().replace('https://', ''), 'token'], token);
}

