'use strict';

const { Transform } = require('stream');

class LineStream extends Transform {
    constructor(options) {
      super();
      this._buffer = '';
    }

    _transform(chunk, encoding, callback) {
        this._buffer += chunk.toString('utf8');

        const lines = this._buffer.split('\n');
        this._buffer = lines.pop(); // maybe incomplete line

        for (const line of lines) {
            this.emit('line', line);
        }

        callback();
    }

    _flush(callback) {
      if (this._buffer) this.emit('line', this.buff_bufferer);
      callback();
    }
}

exports = module.exports = LineStream;
