'use strict';

exports = module.exports = {
    question
};

const readline = require('node:readline/promises'),
    { Writable } = require('node:stream');

async function question(query, options) {
    const output = options.noEchoBack
        ? new Writable({ write: function (chunk, encoding, callback) { callback(); } })
        : process.stdout;
    process.stdin.setRawMode(options.noEchoBack); // raw mode gives each keypress as opposd to line based cooked mode
    const rl = readline.createInterface({ input: process.stdin, output });
    if (options.noEchoBack) process.stdout.write(query);
    const answer = await rl.question(query, options);
    rl.close();
    if (options.noEchoBack) process.stdout.write('\n');
    return answer;
}
