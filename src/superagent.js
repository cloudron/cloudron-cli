'use strict';

exports = module.exports = {
    get,
    put,
    post,
    patch,
    del,
    options,
    request
};

// IMPORTANT: do not require box code here . This is used by migration scripts
const assert = require('assert'),
    consumers = require('node:stream/consumers'),
    debug = require('debug')('box:superagent'),
    fs = require('fs'),
    http = require('http'),
    https = require('https'),
    path = require('path'),
    safe = require('safetydance');

class Request {
    #boundary;
    #redirectCount;
    #retryCount;
    #timer;
    #body;
    #okFunc;
    #options;
    #url;

    constructor(method, url) {
        assert.strictEqual(typeof url, 'string');

        this.#url = new URL(url);
        this.#options = {
            method,
            headers: {},
            signal: null // set for timeouts
        };
        this.#okFunc = ({ status }) => status >=200 && status <= 299;
        this.#timer = { timeout: 0, id: null, controller: null };
        this.#retryCount = 0;
        this.#body = Buffer.alloc(0);
        this.#redirectCount = 5;
        this.#boundary = null; // multipart only
    }

    async _handleResponse(url, response) {
        // const contentLength = response.headers['content-length'];
        // if (!contentLength || contentLength > 5*1024*1024*1024) throw new Error(`Response size unknown or too large: ${contentLength}`);
        const [consumeError, data] = await safe(consumers.buffer(response)); // have to drain response
        if (consumeError) throw new Error(`Error consuming body stream: ${consumeError.message}`);
        if (!response.complete) throw new Error('Incomplete response');
        const contentType = response.headers['content-type'];

        const result = {
            url: new URL(response.headers['location'] || '', url),
            status: response.statusCode,
            headers: response.headers,
            body: null,
            text: null
        };

        if (contentType?.includes('application/json')) {
            result.text = data.toString('utf8');
            if (data.byteLength !== 0) result.body = safe.JSON.parse(result.text) || {};
        } else if (contentType?.includes('application/x-www-form-urlencoded')) {
            result.text = data.toString('utf8');
            const searchParams = new URLSearchParams(data);
            result.body = Object.fromEntries(searchParams.entries());
        } else if (!contentType || contentType.startsWith('text/')) {
            result.body = data;
            result.text = result.body.toString('utf8');
        } else {
            result.body = data;
            result.text = `<binary data (${data.byteLength} bytes)>`;
        }

        return result;
    }

    async _makeRequest(url) {
        return new Promise((resolve, reject) => {
            const proto = url.protocol === 'https:' ? https : http;
            const request = proto.request(url, this.#options); // ClientRequest

            request.on('error', reject); // network error, dns error
            request.on('response', async (response) => {
                const [error, result] = await safe(this._handleResponse(url, response));
                if (error) reject(error); else resolve(result);
            });

            request.write(this.#body);

            request.end();
        });
    }

    async _start() {
        let error;

        for (let i = 0; i < this.#retryCount+1; i++) {
            if (this.#timer.timeout) this.#timer.id = setTimeout(() => this.#timer.controller.abort(), this.#timer.timeout);
            debug(`${this.#options.method} ${this.#url.toString()}` + (i ? ` try ${i+1}` : ''));

            let response, url = this.#url;
            for (let redirects = 0; redirects < this.#redirectCount+1; redirects++) {
                [error, response] = await safe(this._makeRequest(url));
                if (error || (response.status < 300 || response.status > 399) || (this.#options.method !== 'GET')) break;
                url = response.url; // follow
            }

            if (!error && !this.#okFunc({ status: response.status })) {
                error = new Error(`${response.status} ${http.STATUS_CODES[response.status]}`);
                Object.assign(error, response);
            }

            if (error) debug(`${this.#options.method} ${this.#url.toString()} ${error.message}`);
            if (this.#timer.timeout) clearTimeout(this.#timer.id);
            if (!error) return response;
        }

        throw error;
    }

    set(name, value) {
        this.#options.headers[name.toLowerCase()] = value;
        return this;
    }

    query(data) {
        Object.entries(data).forEach(([key, value]) => this.#url.searchParams.append(key, value));
        return this;
    }

    redirects(count) {
        this.#redirectCount = count;
        return this;
    }

    send(data) {
        const contentType = this.#options.headers['content-type'];
        if (!contentType || contentType === 'application/json') {
            this.#options.headers['content-type'] = 'application/json';
            this.#body = Buffer.from(JSON.stringify(data), 'utf8');
            this.#options.headers['content-length'] = this.#body.byteLength;
        } else if (contentType === 'application/x-www-form-urlencoded') {
            this.#body = Buffer.from((new URLSearchParams(data)).toString(), 'utf8');
            this.#options.headers['content-length'] = this.#body.byteLength;
        }
        return this;
    }

    timeout(msecs) {
        this.#timer.controller = new AbortController();
        this.#timer.timeout = msecs;
        this.#options.signal = this.#timer.controller.signal;
        return this;
    }

    retry(count) {
        this.#retryCount = Math.max(0, count);
        return this;
    }

    ok(func) {
        this.#okFunc = func;
        return this;
    }

    disableTLSCerts() {
        this.#options.rejectUnauthorized = true;
        return this;
    }

    auth(username, password) {
        this.set('Authorization', 'Basic ' + btoa(`${username}:${password}`));
        return this;
    }

    field(name, value) {
        if (!this.#boundary) this.#boundary = '----WebKitFormBoundary' + Math.random().toString(36).substring(2);

        const partHeader = Buffer.from(`--${this.#boundary}\r\nContent-Disposition: form-data; name="${name}"\r\n\r\n`, 'utf8');
        const partData = Buffer.from(value, 'utf8');
        this.#body = Buffer.concat([this.#body, partHeader, partData, Buffer.from('\r\n', 'utf8')]);

        return this;
    }

    attach(name, filepathOrBuffer) { // this is only used in tests and thus simplistic
        if (!this.#boundary) this.#boundary = '----WebKitFormBoundary' + Math.random().toString(36).substring(2);

        const filename = Buffer.isBuffer(filepathOrBuffer) ? name : path.basename(filepathOrBuffer);
        const partHeader = Buffer.from(`--${this.#boundary}\r\nContent-Disposition: form-data; name="${name}" filename="${filename}"\r\n\r\n`, 'utf8');
        const partData = Buffer.isBuffer(filepathOrBuffer) ? filepathOrBuffer : fs.readFileSync(filepathOrBuffer);
        this.#body = Buffer.concat([this.#body, partHeader, partData, Buffer.from('\r\n', 'utf8')]);

        return this;
    }

    then(onFulfilled, onRejected) {
        if (this.#boundary) {
            const partTrailer = Buffer.from(`--${this.#boundary}--\r\n`, 'utf8');
            this.#body = Buffer.concat([this.#body, partTrailer]);

            this.#options.headers['content-type'] = `multipart/form-data; boundary=${this.#boundary}`;
            this.#options.headers['content-length'] = this.#body.byteLength;
        }

        this._start().then(onFulfilled, onRejected);
    }
}

function get(url) { return new Request('GET', url); }
function put(url) { return new Request('PUT', url); }
function post(url) { return new Request('POST', url); }
function patch(url) { return new Request('PATCH', url); }
function del(url) { return new Request('DELETE', url); }
function options(url) { return new Request('OPTIONS', url); }
function request(method, url) { return new Request(method, url); }
